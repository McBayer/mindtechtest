import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, AreaChart, Area, ComposedChart, Bar } from 'recharts';
import Select from 'react-select'
import DatePicker from 'react-datepicker'

import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-datepicker/dist/react-datepicker.css";
import './index.css';

const backEndUrl = 'http://localhost:8000';

const ChartDataFilterOptions = [
  { value: 'infected', label: 'Infected' },
  { value: 'deceased', label: 'Deceased' },
  { value: 'tested', label: 'Tested' },
  { value: 'recovered', label: 'Recovered' }
];

const ChartTypeFilterOptions = [
  { value: 'line', label: 'Line Chart' },
  { value: 'area', label: 'Area Chart' },
  { value: 'composed', label: 'Composed Chart' }
];


class LineChartBlock extends React.Component {
	render() {
		return (
		<div>
			  <LineChart
				  width={this.props.width * 0.8}
				  height={300}
				  data={this.props.chartData}
				  margin={{
					top: 5,
					right: 30,
					left: 20,
					bottom: 5
				  }}
				>
				  <CartesianGrid strokeDasharray="3 3" />
				  /* @TODO: How can we rotate the label? */
				  <XAxis dataKey="last_update_apify" textAnchor="middle" verticalAnchor="start" />
				  <YAxis />
				  <Tooltip />
				  <Legend />
				  <Line
					type="monotone"
					dataKey={this.props.selectCurrentValue.value}
					stroke="#8884d8"
					activeDot={{ r: 8 }}
				  />
				</LineChart>
		</div>
		);
	}
}

class AreaChartBlock extends React.Component {
	render() {
		return (
		<div>
			<AreaChart width={this.props.width * 0.8} height={250} data={this.props.chartData}
				  margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
				  <defs>
					<linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
					  <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
					  <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
					</linearGradient>
					<linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
					  <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
					  <stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
					</linearGradient>
				  </defs>
				  <XAxis dataKey="last_update_apify" textAnchor="middle" verticalAnchor="start" />
				  <YAxis />
				  <CartesianGrid strokeDasharray="3 3" />
				  <Tooltip />
				  <Area type="monotone" dataKey={this.props.selectCurrentValue.value} stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />
				  <Area type="monotone" dataKey="deceased" stroke="#82ca9d" fillOpacity={1} fill="url(#colorPv)" />
			</AreaChart>
		</div>
		);
	}
}

class ComposedChartBlock extends React.Component {
	render() {
		return (
		<div>
			<ComposedChart width={this.props.width * 0.8} height={250} data={this.props.chartData}>
			  <XAxis dataKey="last_update_apify"/>
			  <YAxis />
			  <Tooltip />
			  <Legend />
			  <CartesianGrid stroke="#f5f5f5" />
			  <Bar dataKey={this.props.selectCurrentValue.value} barSize={20} fill="#413ea0" />
			</ComposedChart>
		</div>
		);
	}
}

class Chart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
	  start_date: new Date("2021-05-01"),
	  end_date: new Date("2021-12-01"),
	  report: [],
      selected_line_chart_option: {value: "infected", label: "Infected"},
      selected_area_chart_option: {value: "infected", label: "Infected"},
      selected_composed_chart_option: {value: "infected", label: "Infected"},
      selected_chart_type: {value: '', label: ''},
      chart_type_option: '',
      width: 300,
    };
    
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleChartTypeChange = this.handleChartTypeChange.bind(this);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    
    this.changePeriod();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
   
  getFieldDate(date) {
	  var month = (date.getMonth() + 1);
	  if (month < 10) {
		  month = "0" + month;
	  }
	  
	  var day = date.getDate();
	  if (day < 10) {
		  day = "0" + day;
	  }
	  
	  return date.getFullYear() + "-" + month + "-" + day;
  }
  
  updateWindowDimensions() {
	this.setState(state => ({width: window.innerWidth}));
  }
   
  changePeriod() {
	  var start_date = this.getFieldDate(this.state.start_date);
	  var end_date = this.getFieldDate(this.state.end_date);
	  
	  // @TODO: Params are in object?
	  axios.get(backEndUrl + '/covid-report?start_date=' + start_date + '&end_date=' + end_date)
	  .then(res => {
	    this.setState(
		  state => ({report: res.data.map(el => el), chart_type_option: 'line'})
		);
		this.handleChartTypeChange({});
		this.updateWindowDimensions();
      })
	  
  }   
  
  handleSelectChange(selectedOption) {
	this.setState(state => ({["selected_" + this.state.chart_type_option + "_chart_option"]: selectedOption }));
  };
  
  handleChartTypeChange(selectedOption) {
	if (!selectedOption.value) {
		selectedOption = ChartTypeFilterOptions[0];
	}
    this.setState(state => ({chart_type_option: selectedOption.value, selected_chart_type: selectedOption}));
  };
  
  handleInputChange(event) {
	this.setState(state => ({[event.target.name]: event.target.value}));
  }
  
  handleDatepickerStartDate(date) {
	  this.setState(state => ({start_date: date }));
  }
  handleDatepickerEndDate(date) {
	  this.setState(state => ({end_date: date }));
  }
  
  
  render() {
	  
    return (
    <div>		
		<form>
			<Container>
			  <Row>
				<Col sm="12" md="4">
					<h4>Date filter</h4>
				</Col>
			  </Row>
			</Container>
			<Container>
			  <Row>
				<Col sm="12" md="4">
				
					<InputGroup className="mb-3">
						<InputGroup.Text id="start-date-addon1">Start</InputGroup.Text>
						
						<DatePicker
						  selected={this.state.start_date} 
						  onChange={(date) => this.handleDatepickerStartDate(date)}
						  onChange={(date) => this.handleDatepickerStartDate(date)}
						  dateFormat="yyyy-MM-dd"
						  className="datepicker-field"
						/>
					</InputGroup>
				</Col>
				<Col sm="12" md="4">
					<InputGroup className="mb-3">
						<InputGroup.Text id="end-date-addon1">End</InputGroup.Text>
						
						<DatePicker
						  selected={this.state.end_date} 
						  onChange={(date) => this.handleDatepickerEndDate(date)}
						  onChange={(date) => this.handleDatepickerEndDate(date)}
						  dateFormat="yyyy-MM-dd"
						  className="datepicker-field"
						/>
					</InputGroup>
				</Col>
				<Col sm="12" md="4">
					<Button variant="primary" onClick={() => this.changePeriod()} >Change period</Button>
				</Col>
			  </Row>
			</Container>
			
		</form>
		
		<hr />
		
		<Container>
			  <Row>
				<Col lg={{span: 4}}>
				    <label className="chart-title">Please select the chart type:</label>
				    <Select options={ChartTypeFilterOptions}
							name="selected_chart_type_option"
							value={this.state.selected_chart_type}
							onChange={this.handleChartTypeChange}></Select>
				</Col>
				<Col lg={{span: 4}}>	
					<label className="chart-title">Please select data type:</label>
				    <Select options={ChartDataFilterOptions}
							name="chart_option"
							value={this.state["selected_" + this.state.chart_type_option + "_chart_option"]}
							onChange={this.handleSelectChange}></Select>		
				
				</Col>
			  </Row>
		</Container>
		
		<hr />
		
		<Container>
			  <Row>
				<Col>
					{ this.state.chart_type_option == "line" ? /* @TODO: is there a simplier way like in Vue <div v-if="">?*/
						<LineChartBlock selectCurrentValue={this.state.selected_line_chart_option}
										width={this.state.width}
										chartData={this.state.report}
					/> : null }

					{ this.state.chart_type_option == "area" ?
						<AreaChartBlock selectCurrentValue={this.state.selected_area_chart_option} 
								  width={this.state.width}
								  chartData={this.state.report}
				    /> : null }
		 
					{ this.state.chart_type_option == "composed" ?
						<ComposedChartBlock selectCurrentValue={this.state.selected_composed_chart_option} 
											width={this.state.width}
											chartData={this.state.report}
					/> : null }
				</Col>
			</Row>
		</Container>
    </div>
    );
  }
}

class App extends React.Component {
	render() {
		return (
		<div className="covid-report">
			<Container>
				<Row>
					<Col className="header text-center">
						<img src="https://mindtechapps.com/_next/image?url=%2Fmindtech-logo.svg&w=384&q=75" alt="" />
						<h1>Covid Report</h1>
					</Col>
				</Row>
			</Container>
			<Chart />
		</div>
		);
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('root')
);
