<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$CORS_ORIGIN_ALLOWED = "http://localhost:3000";    

header("Access-Control-Allow-Origin: {$CORS_ORIGIN_ALLOWED}");
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Accept');


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('charts', function () {
	return view('charts', ['name' => 'James']);
});

// http://localhost:8000/covid-report?start_date=2021-05-01&end_date=2021-12-01
$router->get('covid-report', 'CovidReportController@loadReport');
