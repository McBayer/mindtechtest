<?php

date_default_timezone_set("Europe/Budapest");

/**
 * SQL
 */
include_once("class_sql.php");
$sql = SQL_Connection::instance();





$url = 'https://api.apify.com/v2/datasets/Gm6qjTgGqxkEZTkuJ/items?format=json&clean=1';
$curl = curl_init($url);
curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
@curl_setopt($curl,CURLOPT_FOLLOWLOCATION, 1);
$apify_covid = curl_exec($curl);
curl_close($curl);

$covid_statistics = json_decode($apify_covid);
print_r($covid_statistics);

$sources = [];
$insert_query = [];
foreach($covid_statistics as $key => $row) {
	
	$infected = intval($row->infected);
	$active_infected = intval($row->activeInfected);
	$deceased = intval($row->deceased);
	$recovered = intval($row->recovered);
	$quarantined = intval($row->quarantined);
	$tested = intval($row->tested);
	$source = $row->sourceUrl;
	$last_updated_source = date("Y-m-d H:i:s", strtotime($row->lastUpdatedAtSource));
	$last_updated_apify = date("Y-m-d H:i:s", strtotime($row->lastUpdatedAtApify));
	
	if (!array_key_exists($source, $sources)) {
		$source_table 	= $sql->select_query("SHOW TABLE STATUS LIKE 'mtch_source'");
		$source_id 		= $source_table->Auto_increment;
		$sql->query("INSERT INTO mtch_source (id, source) VALUES (" . $source_id . ", " . $sql->str2sql($source) . ")");
		$sources[$source] = $source_id;
	}
	
	$insert_query[] = "(" . $sql->str2sql($infected) . ", " . $sql->str2sql($active_infected) . ", " . $sql->str2sql($deceased) . ",
						 " . $sql->str2sql($recovered) . ", " . $sql->str2sql($quarantined) . ",
						 " . $sql->str2sql($tested) . ", " . $sql->str2sql($sources[$source]) . ",
						 " . $sql->str2sql($last_updated_source) . ", " . $sql->str2sql($last_updated_apify) . "
						 )";
	
	if ($key > 0 && $key % 100 == 0) {
		$query = "INSERT INTO mtch_covid_report 
						(infected, active_infected, deceased, recovered, quarantined, tested,
						 source_id, last_update_source, last_update_apify)
				 VALUES 
				" . join(", ", $insert_query);
		echo $query . "\n";
		$sql->query($query);
		$insert_query = [];
	}
}

if (count($insert_query)) {
	$query = "INSERT INTO mtch_covid_report 
					(infected, active_infected, deceased, recovered, quarantined, tested,
					 source_id, last_update_source, last_update_apify)
			 VALUES 
			" . join(", ", $insert_query);
	$sql->query($query);
}
