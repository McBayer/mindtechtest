import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


const data = [
  {
	name: 'Page A',
	uv: 4000,
	pv: 2400,
	amt: 2400,
  },
  {
	name: 'Page B',
	uv: 3000,
	pv: 1398,
	amt: 2210,
  },
  {
	name: 'Page C',
	uv: 2000,
	pv: 9800,
	amt: 2290,
  },
  {
	name: 'Page D',
	uv: 2780,
	pv: 3908,
	amt: 2000,
  },
  {
	name: 'Page E',
	uv: 1890,
	pv: 4800,
	amt: 2181,
  },
  {
	name: 'Page F',
	uv: 2390,
	pv: 3800,
	amt: 2500,
  },
  {
	name: 'Page G',
	uv: 3490,
	pv: 4300,
	amt: 2100,
  },
];

class Chart extends React.PureComponent {
  
  changeInterval() {
	  console.log("changeInterval");
  }
  
  showSelectedData() {
	  console.log("showSelectedData");
  }
  
  render() {
	return (
	<div>
		<h2>Please select:</h2>
		
		<select onChange={() => this.showSelectedData()}>
			<option value="">Infected</option>
			<option value="">Deceased</option>
			<option value="">Recovered</option>
		</select>
	
	  <LineChart
		  width={500}
		  height={300}
		  data={data}
		  margin={{
			top: 5,
			right: 30,
			left: 20,
			bottom: 5
		  }}
		>
		  <CartesianGrid strokeDasharray="3 3" />
		  <XAxis dataKey="name" />
		  <YAxis />
		  <Tooltip />
		  <Legend />
		  <Line
			type="monotone"
			dataKey="pv"
			stroke="#8884d8"
			activeDot={{ r: 8 }}
		  />
		  <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
		</LineChart>
	</div>
	);
  }
}



class ChartContainer extends React.Component {
	render() {
		
		return (
		<div className="game">
			<div>Helloka</div>
			
			<Chart></Chart>
		</div>
		);
	}
}

/*
ReactDOM.render(
  <ChartContainer />,
  document.getElementById('root')
);*/

//export default function App() {
class App extends React.Component {
  constructor(props) {
	  super(props);
	  this.state = {
		  start_date: "2021-05-01",
		  end_date: "2021-12-01",
	  };
	  
	  this.handleChange = this.handleChange.bind(this);
  }
  
  changeInterval() {
	  console.log(this.state.start_date);
	  console.log(this.state.end_date);
	  
	  //axios.post('http://localhost:8000/get-covid-report.php', { start_date: this.state.start_date, end_date: this.state.end_date })
	  axios.post('data.html', { start_date: this.state.start_date, end_date: this.state.end_date })
	  .then(res => {
		console.log(res);
		console.log(res.data);
	  })
	  
  }
  
  handleChange(event) {
    console.log(event.target);
    //this.setState({value: event.target.value});
  }
  
  render() {
	  return (
	  <div className="moda">
		<h1>EGCS</h1>
		
		<form>
			<label>Start:</label>
			<input type="text" value={this.state.start_date} name="start_date" onChange={this.handleChange}/>
			<label>End:</label>
			<input type="text" value={this.state.end_date} name="end_date" onChange={this.handleChange}/>
			<button type="button" onClick={() => this.changeInterval()} >Change interval</button>
		</form>
		
		<Chart/>
	  </div>
	  );
  }
}


ReactDOM.render(
  <App />,
  document.getElementById('content')
);
