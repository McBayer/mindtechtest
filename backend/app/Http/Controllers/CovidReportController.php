<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class CovidReportController extends Controller
{
    public function loadReport(Request $request)
    {
		$start_date	= $request->query('start_date');
		$end_date 	= $request->query('end_date');
		
		// @TODO: How can i do the escaping?
        $results = app('db')->select("SELECT DISTINCT infected, active_infected, deceased, recovered, quarantined, 
											 tested, DATE(last_update_apify) AS last_update_apify
									  FROM mtch_covid_report 
									  WHERE last_update_apify BETWEEN '" . $start_date . "' AND '" . $end_date . "'");
        
        return json_encode($results);
    }

    //
}
